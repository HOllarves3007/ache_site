angular.module('acheSite.routes', [])
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('root', {
                url: '/',
                views:{
                    'root-view': {
                        templateUrl: 'modules/layout/layout.html',
                        controller: "LayoutCtrl"
                    }
                }
            })
            .state('root.about', {
                title: "Me?",
                url:'me',
                views:{
                    'content':{
                        templateUrl:'modules/me/about.html',
                        controller:"AboutCtrl",
                        controllerAs:"me"
                    }
                }
            })
            .state('root.education', {
                title:"Education",
                url:"education",
                views:{
                    'content':{
                        templateUrl:'modules/education/education.html',
                        controller:"EducationCtrl",
                        controllerAs:"education"
                    }
                }
        });
        $urlRouterProvider.otherwise('/');
    })
    .run(function($rootScope){
        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
            if(toState.title){
                $rootScope.title = toState.title;
            }
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });
    });
